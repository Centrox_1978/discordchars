﻿using System;
using System.Linq;
using System.Net.Http.Headers;
using TextCopy;

namespace Discord_chars
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true) // Loop indefinitely
            {
                Console.WriteLine("Enter input:"); // Prompt
                string line = Console.ReadLine(); // Get string from user
                if (line == "exit") // Check string
                {
                    break;
                }

                var chars = line.Replace(",", string.Empty).Replace("'", string.Empty).ToCharArray();

                var magic = chars.Select(c => c.Equals('.') | c.Equals(' ') ? ":blue_square: " : 
                                isNumber(c.ToString())  ? ConvertNumber(c) : 
                                IsExclamationOrQuestionMark(c) ? ConvertQuestionMarkExclamationMark (c) : 
                                (string)$":regional_indicator_{c.ToString().ToLower()}: ").ToList<string>();

                Console.WriteLine("magic string:  "); // Report output
                var str = string.Join("", magic);
                Console.WriteLine(str);
                ClipboardService.SetText(str);
            }
        }

        private static string ConvertNumber(char number)
        {
            switch (number)
            {
                case '0': { return ":zero:"; }
                case '1': { return ":one:"; }
                case '2': { return ":two:"; }
                case '3': { return ":three:"; }
                case '4': { return ":four:"; }
                case '5': { return ":five:"; }
                case '6': { return ":six:"; }
                case '7': { return ":seven:"; }
                case '8': { return ":eight:"; }
                case '9': { return ":nine:"; }
                default: return number.ToString();
            }
        }

        private static bool isNumber(string number)
        {
            return long.TryParse(number, out var number1);
        }

        private static bool isEnter(char enter)
        {
            return (enter == (char)13);
        }

        private static bool IsExclamationOrQuestionMark(char c)
        {
            return c.Equals('?') || c.Equals('!');
        }

        private static string ConvertQuestionMarkExclamationMark(char c)
        {
            switch (c)
            {
                case '!': { return ":exclamation:"; }
                case '?': { return ":question:"; }
                default: return string.Empty;
            }
        }
    }
}
